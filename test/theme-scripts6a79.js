
// Timeline dots content slider

jQuery(document).ready(function() {
	
	jQuery('#timeline li').click(function() {
		jQuery("ul#timeline li").removeClass('timeline-item-selected');
		jQuery("ul#timeline-descriptions li").removeClass("timeline-visible");
		var timelineitem = jQuery(this).data('timeline_tag');
		jQuery(this).addClass('timeline-item-selected rotateninety');
		jQuery("ul#timeline-descriptions li[data-timeline_tag='" + timelineitem + "']").addClass('timeline-visible');
	});


	// Move the staff left or right when clicking the prev or next buttons

	$('#timeline-next').click(function() {
		$('.timeline-item.timeline-item-selected').next('.timeline-item').click();
	});

	$('#timeline-prev').click(function() {
		$('.timeline-item.timeline-item-selected').prev('.timeline-item').click();
	});

});