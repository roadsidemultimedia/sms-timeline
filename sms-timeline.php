<?php
/*
Plugin Name: Timeline Slider
Plugin URI: http://www.roadsidemultimedia.com
Description: Timeline Slider photo navigation. 
Author: Curtis Grant
PageLines: true
Version: 1.0.4
Section: true
Class Name: SMSTimeline
Filter: slider, gallery, component
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-timeline
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;

global $TL_num;
$TL_num = 0;

class SMSTimeline extends PageLinesSection {

  function section_styles(){
    wp_enqueue_script( 'jquery-cycle', $this->base_url.'/js/jquery.cycle.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_style( 'timeline-css', $this->base_url.'/css/sms-timeline.css');
  }

  function section_opts(){


    $options = array();    
    
    $options[] =
      array(
        'type'  => 'multi',
        'col'   => 1,
        'title' => 'Timeline Options',
        'opts'  => array(
          array(
            'type'      => 'select',
            'key'     => 'TL_arrow_nav',
            'label'     => 'Arrow Nav (coming soon)',
            'col' => 1,
            'opts'      => array(
              'yes'   => array('name' => 'Yes'),
              'no'  => array('name' => 'No'),
                  )
              ),
          )
  );
$options[] =
      array(
        'key'   => 'array',
        'type'    => 'accordion', 
        'col'   => 2,
        'title' => 'Slides',
        'opts_cnt'  => 4,
        'title'   => __('Timeline Slides', 'pagelines'), 
        'opts'  => array(
          array(
          'key'   => 'TL_image',
          'label'   => __( 'Image', 'pagelines' ),
          'type'    => 'image_upload',
          ),
          array(
              'key'   => 'TL_image_border_color',
              'label'   => __( 'Image Border Color', 'pagelines' ),
              'type'    => 'color',
                ),
          array(
              'key'   => 'TL_hdr',
              'label'   => __( 'Timeline Hdr', 'pagelines' ),
              'type'    => 'text',
                ),
          array(
              'key'   => 'TL_text',
              'label'   => __( 'Timeline Text', 'pagelines' ),
              'type'    => 'textarea',
                ),
          )
      );

    
    return $options;

  }


  function get_content( $array ){
    
    
    $out = '';
    $num = 1;
    $nav[] = array();
    $slides[] = array();
    if( is_array( $array ) ){
      foreach( $array as $key => $item ){
        $tlhdr = pl_array_get( 'TL_hdr', $item );
        $tlimage = pl_array_get( 'TL_image', $item ); 
        $tlimagebgcolor = pl_array_get( 'TL_image_border_color', $item ); 
        if($num == 1) { $tclass='timeline-item-selected';} else { $tclass='';}
        if( $tlhdr ){
         
          $topimages .= sprintf(
            '<li class="timeline-item %s" data-timeline_tag="%s"><h2>%s</h2><img src="%s" style="border-color:#%s;"></li>',
            $tclass,
            $num,
            $tlhdr,
            $tlimage,
            $tlimagebgcolor
          );
        }
      $num++;
      }
    }
    
    
    return $topimages;
  }
  function get_slides( $array ){
    
    
    $out = '';
    $num = 1;
    $nav[] = array();
    $slides[] = array();
    if( is_array( $array ) ){
      foreach( $array as $key => $item ){
        $tlhdr = pl_array_get( 'TL_hdr', $item );
        $tltext = pl_array_get( 'TL_text', $item );
        $tlimage = pl_array_get( 'TL_image', $item ); 
        $tltext = $tltext; 
        $tltext = wpautop($tltext);
  if($num == 1) { $tclass='timeline-visible';} else { $tclass='';}
        if( $tlhdr ){
         
          $out .= sprintf(
            '<li class="%s" data-timeline_tag="%s"><img src="%s"><h2>%s</h2>%s</li>',
            $tclass,
            $num,
            $tlimage,
            $tlhdr,
            $tltext
          );
        }
      $num++;
      }
    }
    
    
    return $out;
  }

   function section_template( ) { 
    
    $array = $this->opt('array');
  global $TL_num;
   if(!$TL_num) {
     $TL_num = 1;
   }
   ?>
  <div id="sms-timeline" class="container-wrapper">
  <div class="container">
    <div class="grid">
      <div class="unit span-grid">
          <ul id="timeline">
            <?php

              $topimages = $this->get_content( $array );

              echo $topimages;

            ?>
          </ul>
          <ul id="timeline-descriptions">
            <?php

              $slides = $this->get_slides( $array );

              echo $slides;

            ?>
          </ul>
  <div id="timeline-prev"><span class="icon icon-angle-left"></span></div>
        <div id="timeline-next"><span class="icon icon-angle-right"></span></div>
      </div>
    </div>
  </div>
</div>

  <script type="text/javascript">
    jQuery(document).ready(function($) {
    
    jQuery('#timeline li').click(function() {
        jQuery("ul#timeline li").removeClass('timeline-item-selected');
        jQuery("ul#timeline-descriptions li").removeClass("timeline-visible");
        var timelineitem = jQuery(this).data('timeline_tag');
        jQuery(this).addClass('timeline-item-selected');
        jQuery("ul#timeline-descriptions li[data-timeline_tag='" + timelineitem + "']").addClass('timeline-visible');
    });


    // Move the staff left or right when clicking the prev or next buttons

    $('#timeline-next').click(function() {
        $('.timeline-item.timeline-item-selected').next('.timeline-item').click();
    });

    $('#timeline-prev').click(function() {
        $('.timeline-item.timeline-item-selected').prev('.timeline-item').click();
    });

});
  </script>

<?php 
$TL_num++;
}


}